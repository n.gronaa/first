<?php
declare(strict_types=1);

///**
// * My first function
// *
// * @param string $string
// * @return string
// */
//function upper(string $string): string
//{
//    return strtoupper($string);
//}

// функція додавання
/**
 * @param int $a
 * @param int $b
 * @return int
 */
function addition(int $a, int $b): int
{
    return $a + $b;
}

// функція віднімання
/**
 * @param int $a
 * @param int $b
 * @return int
 */
function subtraction(int $a, int $b): int
{
    return $a - $b;
}

// функція множення
/**
 * @param int $a
 * @param int $b
 * @return int
 */
function multiplication(int $a, int $b): int
{
    return $a * $b;
}

// функція ділення
/**
 * @param int $a
 * @param int $b
 * @return int
 */
function division(int $a, int $b): int
{
    return $a / $b;
}
